const state = {
    allData: [],
    data: []
}

const getters = {
    getData: (state) => state.data,
    getUserID: (state) => userId => state.allData.find((e) => e.id === userId),
    getDataByUserName: (state) => name => state.allData.find((e) => e.username === name)
}

const actions = {
    addData({ commit }, employees) {
        commit('SET_DATE', employees)
    },
    deleteUser({ commit }, userId) {
        commit('REMOVE_RECORD', userId)
    }
}

const mutations = {
    SET_DATE(state, data) {
        if (state.data.length == 0) {
            state.allData = data.allData;
            state.data = data.data;
        }
    },
    REMOVE_RECORD(state, userId) {
        const userIndex = state.data.findIndex((u) => u.id === userId)
        state.data.splice(userIndex, 1)
    }
}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}