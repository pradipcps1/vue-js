import { createRouter, createWebHistory } from 'vue-router'
import AppLayout from '@/views/layout/AppLayout.vue';
import HomeView from '@/views/HomeView.vue';
import UserView from '@/views/UserView.vue';
import UserEdit from '@/views/UserEdit.vue';
import Error404 from '@/views/Error404.vue';

const routes = [
    {
        path: '',
        component: AppLayout,
        children: [
            {
                path: '',
                name: 'home',
                component: HomeView
            },
            {
                path: '/:name',
                name: 'userView',
                component: UserView
            },
            {
                path: '/:name/edit',
                name: 'userEdit',
                component: UserEdit
            },
            { 
                path: '/:pathMatch(.*)*',
                component: Error404
            }
        ]
    }
];

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
});

export default router