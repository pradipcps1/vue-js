import { createApp } from 'vue'
import App from './App.vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import { store } from './module/index'
import router from './views/router';

const app = createApp(App);
app.use(router);
app.use(store);
app.mount('#app');